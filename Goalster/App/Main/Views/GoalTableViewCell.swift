//
//  GoalTableViewCell.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import UIKit

class GoalTableViewCell: UITableViewCell {
    static let height: CGFloat = 160

    var goal: Goal? {
        didSet {
            if let goal = goal {
                setGoal(goal)
            }
        }
    }

    lazy var titleView = UILabel()
    lazy var descriptionView = UILabel()

    lazy var rewardContainer = UIView()
    lazy var rewardTitleView = UILabel()
    lazy var rewardTrophyView = UIImageView()
    lazy var rewardPointsView = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        contentView.addSubview(titleView)
        contentView.addSubview(descriptionView)
        contentView.addSubview(rewardContainer)

        rewardContainer.addSubview(rewardTitleView)
        rewardContainer.addSubview(rewardTrophyView)
        rewardContainer.addSubview(rewardPointsView)

        let smallSpacing: CGFloat = 8

        contentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(GoalTableViewCell.height)
        }

        titleView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(smallSpacing)
        }

        descriptionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(titleView.snp.bottom).offset(smallSpacing)
        }

        rewardContainer.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(descriptionView.snp.bottom).offset(smallSpacing)
            make.bottom.equalToSuperview()
        }

        rewardTitleView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(smallSpacing)
        }

        rewardTrophyView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.equalToSuperview().dividedBy(2)
            make.top.equalTo(rewardTitleView.snp.bottom).offset(smallSpacing)
            make.height.equalTo(24)
        }

        rewardPointsView.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.width.equalToSuperview().dividedBy(2)
            make.top.equalTo(rewardTitleView.snp.bottom).offset(smallSpacing)
        }

        setStyle()
    }

    private func setGoal(_ goal: Goal) {
        titleView.text = goal.title
        descriptionView.text = goal.description
        rewardTrophyView.image = goal.reward.trophy.image

        let pointsFormat = "num_points".localized
        let pointsString = String(format: pointsFormat, String(goal.reward.points))
        rewardPointsView.text = pointsString
        rewardPointsView.textColor = goal.type?.color

        rewardTitleView.text = "rewards".localized

        rewardContainer.layer.shadowRadius = 1
        rewardContainer.layer.shadowOffset = .init(width: 1, height: 1)
        rewardContainer.layer.shadowOpacity = 0.5
        rewardContainer.layer.shadowColor = goal.type?.color.cgColor

    }

    private func setStyle() {
        self.contentView.backgroundColor = UIColor.secondarySystemBackground
        self.rewardContainer.backgroundColor = UIColor.tertiarySystemBackground

        self.contentView.layer.cornerRadius = 10
        self.rewardContainer.layer.cornerRadius = 10

        self.titleView.font = titleView.font.withSize(20)
        self.descriptionView.font = descriptionView.font.withSize(16)

        self.rewardTitleView.font = rewardTitleView.font.withSize(20)
        self.rewardPointsView.font = rewardPointsView.font.withSize(16)

        self.titleView.textAlignment = .center
        self.descriptionView.textAlignment = .center

        self.rewardTitleView.textAlignment = .center
        self.rewardPointsView.textAlignment = .center
        self.rewardTitleView.textAlignment = .center

        self.rewardTrophyView.contentMode = .scaleAspectFit
    }
}
