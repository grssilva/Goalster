//
//  MainCoordinator.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class MainCoordinator: Coordinator {
    var nextCoordinator: Coordinator = ProgressCoordinator()

    lazy var viewModel = MainViewModel()
    lazy var view  = MainView(viewModel: viewModel)
    lazy var viewController = ViewController(view: view, viewModel: viewModel)
    lazy var navigationController = UINavigationController(rootViewController: viewController)

    let disposeBag = DisposeBag()

    init(window: UIWindow?) {
        window?.rootViewController = load()
        window?.makeKeyAndVisible()
    }

    func load() -> UIViewController {
        viewModel.finished.observeOn(MainScheduler.instance)
            .filter { $0 == true }
            .subscribe { [weak self] _ in
                self?.goToNextCoordinator()
            }
            .disposed(by: disposeBag)

        return navigationController
    }
}

extension MainCoordinator {
    func goToNextCoordinator() {
        self.navigationController.pushViewController(nextCoordinator.load(), animated: true)
    }
}
