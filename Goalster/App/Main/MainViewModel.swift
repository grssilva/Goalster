//
//  MainViewModel.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import RxSwift
import RxCocoa

class MainViewModel: ViewModel {
    var goals = BehaviorRelay<[Goal]>(value: [])

    let goalRepository: GoalRepository
    let stepsRepository: StepsRepository

    let title: String = "Goalster"
    var finished = BehaviorRelay<Bool>(value: false)
    let disposeBag = DisposeBag()

    init(goalRepository: GoalRepository = LiveGoalRepository.shared,
         stepsRepository: StepsRepository = HealthKitStepsRepository.shared) {
        self.goalRepository = goalRepository
        self.stepsRepository = stepsRepository
    }

    func start() {
        finished.accept(false)
        getGoals()
    }

    func stop() {
    }

    func getGoals() {
        goalRepository.getGoals()
            .bind(to: goals)
            .disposed(by: disposeBag)
    }

    func goToProgress() -> Bool {
        if stepsRepository.isEnabled() {
            finished.accept(true)
            return true
        } else {
            stepsRepository.askForPermissions()
                .subscribeOn(MainScheduler.instance)
                .subscribe { success in
                    self.finished.accept(success)
                }.disposed(by: disposeBag)
        }
        return false
    }
}
