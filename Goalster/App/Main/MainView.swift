//
//  MainView.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit

class MainView: UIView {
    private static let reuseIdentifierCell = "Cell"

    let viewModel: MainViewModel
    let disposeBag = DisposeBag()

    // views
    lazy var progressButton = UIButton()
    lazy var tableView = UITableView(frame: CGRect.zero, style: .plain)

    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(frame: CGRect.zero)
        setup()
    }

    @available(*, unavailable) required init?(coder: NSCoder) {
        fatalError("Disabled init")
    }

    func setup() {
        tableView.register(GoalTableViewCell.self, forCellReuseIdentifier: MainView.reuseIdentifierCell)
        tableView.separatorStyle = .none
        tableView.rowHeight = GoalTableViewCell.height
        tableView.allowsSelection = false

        addSubview(progressButton)
        addSubview(tableView)

        progressButton.snp.makeConstraints { make in
            make.bottom.equalTo(safeArea.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(64)
        }

        tableView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(progressButton.snp.top)
        }

        progressButton.setTitle("go_to_progress".localized, for: .normal)
        progressButton.backgroundColor = UIColor.green
        progressButton.setTitleColor(UIColor.black, for: .normal)
        progressButton.accessibilityIdentifier = "Progress"

        progressButton.rx.tap.bind { _ in
            _ = self.viewModel.goToProgress()
        }.disposed(by: disposeBag)

        viewModel.goals.bind(to: tableView.rx.items(cellIdentifier: MainView.reuseIdentifierCell,
            cellType: GoalTableViewCell.self)) { _, data, cell in
            cell.goal = data
        }.disposed(by: disposeBag)
    }
}
