//
//  ProgressViewModel.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import RxSwift
import RxCocoa

class ProgressViewModel: ViewModel {
    var stepData = BehaviorRelay<StepsData?>(value: nil)
    var goals = BehaviorRelay<[Goal]>(value: [])
    var points = BehaviorRelay<Int>(value: 0)
    var stepsToNextReward = BehaviorRelay<Double?>(value: nil)
    var goalsEarned = BehaviorRelay<[Goal]>(value: [])

    var rewards = BehaviorRelay<[Reward]>(value: [])

    let goalRepository: GoalRepository
    let stepsRepository: StepsRepository

    let title = "Progress"
    let disposeBag = DisposeBag()

    init(goalRepository: GoalRepository = LiveGoalRepository.shared,
         stepsRepository: StepsRepository = HealthKitStepsRepository.shared) {
        self.goalRepository = goalRepository
        self.stepsRepository = stepsRepository
    }

    func start() {
        goalRepository.getGoals()
            .bind(to: goals)
            .disposed(by: disposeBag)

        stepsRepository.stepCount
            .subscribeOn(MainScheduler.instance)
            .subscribe { event in
            if let stepData = event.element! {
                let goals = self.goals.value
                let stepGoals = goals.filter { $0.type == .step }
                self.refreshData(stepData: stepData, stepGoals: stepGoals)
            }
        }.disposed(by: disposeBag)

        stepsRepository.update()
    }

    func refreshData(stepData: StepsData, stepGoals: [Goal]) {
        self.stepData.accept(stepData)

        let steps = stepData.steps

        // calculate the goals
        let goalsEarned = getEarnedGoals(steps: steps, allGoals: stepGoals)
        self.goalsEarned.accept(goalsEarned)

        //calculate the rewards
        let rewardsEarned = goalsEarned.map { $0.reward }
        self.rewards.accept(rewardsEarned)

        // calculate the points
        let points = getPoints(goals: goalsEarned)
        self.points.accept(points)

        // calculate steps to next reward
        if let nextGoal = getNextGoal(steps: steps, allGoals: stepGoals) {
            let stepsToNextGoal = nextGoal.goal - steps
            self.stepsToNextReward.accept(stepsToNextGoal)
        } else {
            self.stepsToNextReward.accept(nil)
        }

    }

    func getEarnedGoals(steps: Double, allGoals: [Goal]) -> [Goal] {
        var goalsEarned: [Goal] = []
        for goal in allGoals where goal.goal <= steps {
                goalsEarned.append(goal)
        }
        return goalsEarned
    }

    func getNextGoal(steps: Double, allGoals: [Goal]) -> Goal? {
        var goalsMissing: [Goal] = []
        for goal in allGoals where goal.goal > steps {
            goalsMissing.append(goal)
        }
        if goalsMissing.count > 0 {
            goalsMissing.sort { $0.goal < $1.goal }
            return goalsMissing[0]
        } else {
            return nil
        }
    }

    func getPoints(goals: [Goal]) -> Int {
        var points = 0
        for goal in goals {
            points += goal.reward.points
        }
        return points
    }
}
