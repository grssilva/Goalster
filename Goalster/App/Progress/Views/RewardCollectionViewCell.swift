//
//  RewardCollectionViewCell.swift
//  Goalster
//
//  Created by Guilherme Silva on 03/10/2020.
//

import UIKit

class RewardCollectionViewCell: UICollectionViewCell {
    var reward: Reward? {
        didSet {
            if let reward = reward {
                setReward(reward)
            }
        }
    }

    lazy var imageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        contentView.addSubview(imageView)

        contentView.snp.makeConstraints { make in
            make.size.equalTo(48)
        }

        imageView.snp.makeConstraints { make in
            make.size.equalToSuperview()
        }
    }

    func setReward(_ reward: Reward) {
        imageView.image = reward.trophy.image
    }
}
