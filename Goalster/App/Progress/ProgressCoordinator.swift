//
//  ProgressCoordinator.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import UIKit

class ProgressCoordinator: Coordinator {
    lazy var viewModel = ProgressViewModel()
    lazy var view = ProgressView(viewModel: viewModel)
    lazy var controller = ViewController(view: view, viewModel: viewModel)

    func load() -> UIViewController {
        return controller
    }
}
