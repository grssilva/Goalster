//
//  ProgressView.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit

class ProgressView: UIView {
    private static let reuseIdentifierCell = "Cell"

    let viewModel: ProgressViewModel
    let disposeBag = DisposeBag()

    lazy var topTitleView = UILabel()
    lazy var stepsView = UILabel()
    lazy var bottomTitleView = UILabel()

    lazy var pointsView = UILabel()

    lazy var rewardTitleView = UILabel()

    lazy var rewardCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        return collectionView
    }()

    lazy var bottomTextView = UILabel()

    init(viewModel: ProgressViewModel) {
        self.viewModel = viewModel
        super.init(frame: CGRect.zero)
        setup()
    }

    @available(*, unavailable) required init?(coder: NSCoder) {
        fatalError("Disabled init")
    }

    func setup() {
        rewardCollectionView.register(RewardCollectionViewCell.self,
                                      forCellWithReuseIdentifier: ProgressView.reuseIdentifierCell)

        backgroundColor = UIColor.systemBackground

        addSubview(topTitleView)
        addSubview(stepsView)
        addSubview(bottomTitleView)

        addSubview(pointsView)
        addSubview(rewardTitleView)
        addSubview(rewardCollectionView)

        addSubview(bottomTextView)

        topTitleView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(safeArea.top).offset(64)
        }

        stepsView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(topTitleView.snp.bottom).offset(8)
        }

        bottomTitleView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(stepsView.snp.bottom).offset(8)
        }

        pointsView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(bottomTitleView.snp.bottom).offset(12)

        }

        rewardTitleView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(pointsView.snp.bottom).offset(12)

        }

        rewardCollectionView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(64)
            make.trailing.equalToSuperview().offset(-64)
            make.top.equalTo(rewardTitleView.snp.bottom).offset(32)
            make.height.equalTo(64)

        }

        bottomTextView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(rewardCollectionView.snp.bottom).offset(8)
        }
        setupData()

    }

    func setupData() {
        topTitleView.text = "you_walked".localized
        bottomTitleView.text = "steps".localized
        rewardTitleView.text = "your_rewards".localized

        viewModel.stepData
            .observeOn(MainScheduler.instance)
            .subscribe { event in
                if let stepData = event.element! {
                    let valueString = String(format: "%.0f", stepData.steps)
                    self.stepsView.text = valueString
                }
            }
            .disposed(by: disposeBag)

        viewModel.stepsToNextReward
            .observeOn(MainScheduler.instance)
            .subscribe { event in
                if let steps = event.element! {
                    let stepsToNextReward = "steps_to_next_reward".localized
                    let valueString = String(format: stepsToNextReward, steps)
                    self.bottomTextView.text = valueString
                } else {
                    self.bottomTextView.text = "congrats".localized
                }
            }
            .disposed(by: disposeBag)

        viewModel.rewards.bind(to: rewardCollectionView.rx.items(cellIdentifier: ProgressView.reuseIdentifierCell,
            cellType: RewardCollectionViewCell.self)) { _, data, cell in
            cell.reward = data

        }.disposed(by: disposeBag)

        setupStyle()
    }

    func setupStyle() {
        topTitleView.font = topTitleView.font.withSize(18)
        stepsView.font = topTitleView.font.withSize(24)
        bottomTitleView.font = topTitleView.font.withSize(18)

        pointsView.font = topTitleView.font.withSize(18)
        rewardTitleView.font = topTitleView.font.withSize(18)

        bottomTextView.font = topTitleView.font.withSize(18)

        topTitleView.textAlignment = .center
        stepsView.textAlignment = .center
        bottomTitleView.textAlignment = .center
        pointsView.textAlignment = .center

        rewardTitleView.textAlignment = .center
        bottomTextView.textAlignment = .center
    }
}
