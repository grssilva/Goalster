//
//  GoalRepository.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import RxSwift

protocol GoalRepository {
    func getGoals() -> Observable<[Goal]>
}

class LiveGoalRepository: GoalRepository {
    static let shared = LiveGoalRepository()

    let api = GoalLiveApi.shared
    let fileName = "Goals"
    lazy var cache: Cache<String, Goal> = {
        if let diskCache = try? Cache<String, Goal>.loadFromDisk(name: fileName) {
            return diskCache
        } else {
            return Cache<String, Goal>()
        }
    }()

    func saveGoalsInCache(_ goals: [Goal]) {
        cache.removeAll()
        for goal in goals {
            cache.insert(goal, forKey: goal.id)
        }
        try? cache.saveToDisk(name: fileName)
    }

    func getGoals() -> Observable<[Goal]> {
        //If we have the goals in cache, use them, otherwise request from the api
        let goals = cache.values
        if goals.count != 0 {
            return .just(goals)
        } else {
            return api.getGoals().map { response -> [Goal] in
                let goals = self.covertItems(response)
                self.saveGoalsInCache(goals)
                return goals
            }
        }
    }

    func covertItems(_ response: GetGoalsResponse) -> [Goal] {
        var goals: [Goal] = []
        for item in response.items {
            let rewardTrophy = RewardTrophy(rawValue: item.reward.trophy) ?? .bronzeMedal
            let reward = Reward(trophy: rewardTrophy, points: item.reward.points)
            let goalType = GoalType(rawValue: item.type)
            let goal = Goal(id: item.id, title: item.title,
                            description: item.description,
                            type: goalType, goal: Double(item.goal), reward: reward)
            goals.append(goal)
        }
        return goals
    }
}
