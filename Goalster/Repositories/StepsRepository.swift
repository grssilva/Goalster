//
//  StepsRepository.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import RxSwift
import RxCocoa
import HealthKit

protocol StepsRepository {
    var stepCount: BehaviorRelay<StepsData?> { get }

    func isEnabled() -> Bool
    func askForPermissions() -> Observable<Bool>
    func update()
}

class HealthKitStepsRepository: StepsRepository {
    static let shared = HealthKitStepsRepository()

    var stepCount =  BehaviorRelay<StepsData?>(value: nil)

    let healthStore = HKHealthStore()
    private let qualityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!

    func askForPermissions() -> Observable<Bool> {
        let qualityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let healthKitTypes: Set = [qualityType]

        return Observable<Bool>.create { observer in
            self.healthStore.requestAuthorization(toShare: [], read: healthKitTypes) { (success, error) in
                if let error = error {
                    observer.onError(error)
                } else {
                    observer.onNext(success)
                }
                observer.onCompleted()
            }

            return Disposables.create {
            }
        }
    }

    func update() {
        self.retrieveStepCount { (result) in
            self.stepCount.accept(result)
        }
    }

    func isEnabled() -> Bool {
        let status = healthStore.authorizationStatus(for: qualityType)
        switch status {
        case .sharingAuthorized:
            return true
        default:
            return false
        }
    }

    private func retrieveStepCount(completion: @escaping (_ stepRetrieved: StepsData) -> Void) {
        let qualityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!

        //   Get the start of the day
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1

        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)

        //  Perform the Query
        let query = HKStatisticsCollectionQuery(quantityType: qualityType,
                                                quantitySamplePredicate: predicate,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)

        query.initialResultsHandler = { query, results, error in
            guard error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }

            if let myResults = results {
                myResults.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    if let quantity = statistics.sumQuantity() {
                        let steps = quantity.doubleValue(for: HKUnit.count())

                        print("Steps = \(steps)")
                        completion(StepsData(steps: steps, date: now))

                    }
                }
            }
        }

        healthStore.execute(query)
    }
}
