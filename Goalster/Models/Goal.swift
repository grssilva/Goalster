//
//  Goal.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import UIKit

enum GoalType: String, Codable {
    case step = "step"
    case walkingDistance = "walking_distance"
    case runningDistance = "running_distance"
}

struct Goal: Codable, Equatable {
    // swiftlint:disable:next identifier_name
    let id: String
    let title: String
    let description: String
    let type: GoalType?
    let goal: Double
    let reward: Reward

    static func == (lhs: Goal, rhs: Goal) -> Bool {
        return lhs.id == rhs.id &&
            lhs.title == rhs.title &&
            lhs.description == rhs.description &&
            lhs.type == rhs.type &&
            lhs.reward == rhs.reward
    }
}
