//
//  StepsData.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation

struct StepsData: Codable {
    let steps: Double
    let date: Date
}
