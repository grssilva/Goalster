//
//  Reward.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation

enum RewardTrophy: String, Codable {
    case bronzeMedal = "bronze_medal"
    case silverMedal = "silver_medal"
    case goldMedal = "gold_medal"
    case zombieHand = "zombie_hand"
}

struct Reward: Codable, Equatable {
    let trophy: RewardTrophy
    let points: Int

    static func == (lhs: Reward, rhs: Reward) -> Bool {
        return lhs.trophy == rhs.trophy &&
                lhs.points == rhs.points
    }
}
