//
//  GetGoalsResponse.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation

struct GetGoalsResponse: Codable {
    let items: [ItemResponse]
}

struct ItemResponse: Codable {
    // swiftlint:disable:next identifier_name
    let id: String
    let title: String
    let description: String
    let type: String
    let goal: Int
    let reward: RewardResponse
}

struct RewardResponse: Codable {
    let trophy: String
    let points: Int
}
