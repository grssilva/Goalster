//
//  GoalApi.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import RxSwift

protocol GoalApi {
    func getGoals() -> Observable<GetGoalsResponse>
}

class GoalLiveApi {
    static let shared = GoalLiveApi()
    private let session: URLSession

    init(session: URLSession = .shared) {
        self.session = session
    }
}

extension GoalLiveApi: GoalApi {
    func getGoals() -> Observable<GetGoalsResponse> {
        return request(with: makeGetGoalsRequest())

    }

    private func request<T>(with request: URLRequest) -> Observable<T> where T: Decodable {
        return Observable<T>.create { observer in
            let task = self.session.dataTask(with: request) { (data, _, error) in
                do {
//                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
//                        print(dataString)
//                    }
                    let model: T = try JSONDecoder().decode(T.self, from: data ?? Data())
                    observer.onNext(model)
                } catch let error {
                    print(error.localizedDescription)
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }
}

extension GoalLiveApi {
    struct ApiSecret {
        static let scheme = "https"
        static let host = "thebigachallenge.appspot.com"
        static let path = "/_ah/api/myApi/v1/goals"
    }

    func makeGetGoalsRequest() -> URLRequest {
        var components = URLComponents()
        components.host = ApiSecret.host
        components.scheme = ApiSecret.scheme
        components.path = ApiSecret.path

        let url = components.url!
        let request = URLRequest(url: url)
        return request
    }
}
