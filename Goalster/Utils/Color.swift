//
//  Color.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import UIKit

extension GoalType {
    var color: UIColor {
        switch self {
        case .step:
            return .green
        case.walkingDistance:
            return .blue
        case .runningDistance:
            return .red
        }
    }
}
