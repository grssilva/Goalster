//
//  ViewModel.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import RxSwift
import RxRelay

protocol ViewModel {
    // Set this variable to add the title to the view model
    var title: String { get }

    func start()
    func stop()
}

//Optional Implementation
extension ViewModel {
    func start() {
    }

    func stop() {
    }

    var title: String {
        return String(describing: type(of: self))
    }
}
