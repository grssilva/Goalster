//
//  Cache.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation

final class Cache<Key: Hashable, Value> {
    private let wrapped: NSCache<WrappedKey, Entry>
    private let keyTracker = KeyTracker()

    var values: [Value] {
        keyTracker.keys.compactMap(value)
    }

    init(countLimit: Int = 0) {
        wrapped = NSCache<WrappedKey, Entry>()
        wrapped.countLimit = countLimit
    }

    func insert(_ value: Value, forKey key: Key) {
        let entry = Entry(key: key, value: value)
        insert(entry)
    }

    func value(forKey key: Key) -> Value? {
        guard let entry = wrapped.object(forKey: WrappedKey(key)) else {
            return nil
        }

        return entry.value
    }

    func removeValue(forKey key: Key) {
        wrapped.removeObject(forKey: WrappedKey(key))
    }

    func removeAll() {
        keyTracker.keys.removeAll()
        wrapped.removeAllObjects()
    }
}

private extension Cache {
    final class WrappedKey: NSObject {
        let key: Key

        init(_ key: Key) { self.key = key }

        override var hash: Int { return key.hashValue }

        override func isEqual(_ object: Any?) -> Bool {
            guard let value = object as? WrappedKey else {
                return false
            }

            return value.key == key
        }
    }
}

private extension Cache {
    final class Entry {
        let key: Key
        let value: Value
        init(key: Key, value: Value) {
            self.key = key
            self.value = value
        }
    }
}

private extension Cache {
    func entry(forKey key: Key) -> Entry? {
        guard let entry = wrapped.object(forKey: WrappedKey(key)) else {
            return nil
        }

        return entry
    }

    func insert(_ entry: Entry) {
        wrapped.setObject(entry, forKey: WrappedKey(entry.key))
        keyTracker.keys.insert(entry.key)
    }
}

extension Cache {
    subscript(key: Key) -> Value? {
        get { return value(forKey: key) }
        set {
            guard let value = newValue else {
                // If nil was assigned using our subscript,
                // then we remove any value for that key:
                removeValue(forKey: key)
                return
            }

            insert(value, forKey: key)
        }
    }
}

private extension Cache {
    final class KeyTracker: NSObject, NSCacheDelegate {
        var keys = Set<Key>()

        func cache(_ cache: NSCache<AnyObject, AnyObject>,
                   willEvictObject object: Any) {
            guard let entry = object as? Entry else {
                return
            }

            keys.remove(entry.key)
        }
    }
}

// MARK: - Codable extensions
extension Cache.Entry: Codable where Key: Codable, Value: Codable {}

extension Cache: Codable where Key: Codable, Value: Codable {
    convenience init(from decoder: Decoder) throws {
        self.init()

        let container = try decoder.singleValueContainer()
        let entries = try container.decode([Entry].self)
        entries.forEach(insert)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(keyTracker.keys.compactMap(entry))
    }
}

// MARK: - File extensions
extension Cache where Key: Codable, Value: Codable {
    func saveToDisk(
        name: String,
        fileManager: FileManager = .default
    ) throws {
        let fileURL = FileUtils.getURL(name: name, fileManager: fileManager)
        let data = try JSONEncoder().encode(self)
        try data.write(to: fileURL)
    }

    class func loadFromDisk(
        name: String,
        fileManager: FileManager = .default
    ) throws -> Cache {
        let fileURL = FileUtils.getURL(name: name, fileManager: fileManager)
        let data = try Data(contentsOf: fileURL)
        let cache = try JSONDecoder().decode(Cache.self, from: data)
        return cache
    }
}
