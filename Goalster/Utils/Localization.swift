//
//  Localization.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
