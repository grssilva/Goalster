//
//  FileUtils.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation

class FileUtils {
    class func getObjectFromJson<T: Decodable> (type: T.Type, fileName: String = String(describing: (T.self))) -> T? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(T.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }

    class func getArrayFromJson<T: Decodable> (type: T.Type, fileName: String = String(describing: (T.self))) -> [T] {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([T].self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return []
    }

    class func getURL(
        name: String,
        fileManager: FileManager = .default
    ) -> URL {
        let folderURLs = fileManager.urls(
           for: .cachesDirectory,
           in: .userDomainMask
        )

        return folderURLs[0].appendingPathComponent(name)
    }

    class func saveToDisk<T: Encodable>(
        target: T,
        name: String,
        fileManager: FileManager = .default
    ) throws {
        let fileURL = FileUtils.getURL(name: name, fileManager: fileManager)
        let data = try JSONEncoder().encode(target)
        try data.write(to: fileURL)
    }

    class func loadFromDisk<T: Decodable>(
        name: String,
        fileManager: FileManager = .default
    ) throws -> T {
        let fileURL = FileUtils.getURL(name: name, fileManager: fileManager)
        let data = try Data(contentsOf: fileURL)
        let cache = try JSONDecoder().decode(T.self, from: data)
        return cache
    }
}
