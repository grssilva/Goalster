//
//  ViewController.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import UIKit

class ViewController<V: UIView, VM: ViewModel>: UIViewController {
    let viewModel: VM
    let contentView: V

    init(view: V, viewModel: VM) {
        self.contentView = view
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder: NSCoder) {
        fatalError("Disabled init")
    }

    override func loadView() {
        self.view = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.title
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.start()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.stop()

    }
}
