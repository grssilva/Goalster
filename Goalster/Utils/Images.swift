//
//  Images.swift
//  Goalster
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import UIKit

extension RewardTrophy {
    var image: UIImage? {
        switch self {
        case .bronzeMedal:
            return UIImage(named: "BronzeMedal")
        case.silverMedal:
            return UIImage(named: "SilverMedal")
        case .goldMedal:
            return UIImage(named: "GoldMedal")
        case .zombieHand:
            return UIImage(named: "ZombieHand")
        }
    }
}
