//
//  Coordinator.swift
//  Goalster
//
//  Created by Guilherme Silva on 01/10/2020.
//

import Foundation
import UIKit
import RxSwift
import RxRelay

protocol Coordinator {
    func load() -> UIViewController
}

extension Coordinator {
}
