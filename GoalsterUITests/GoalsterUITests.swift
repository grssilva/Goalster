//
//  GoalsterUITests.swift
//  GoalsterUITests
//
//  Created by Guilherme Silva on 02/10/2020.
//

import XCTest

class GoalsterUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        continueAfterFailure = false

        app = XCUIApplication()
        app.launch()
    }

    // Find the progress button on the main scree
    func testProgressButton1() {
        let progressButton = app.buttons["Progress"]
        XCTAssertTrue(progressButton.exists)
        XCTAssertTrue(progressButton.isEnabled)
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
