# Goalster
Goalster iOS App

- Swift programming language
- MVVM-C Architecture (ModelViewViewModel - Coordinator)
- Reactive data handling using RxSwift
- The ViewModels manage data using repositories
- Lib distribution using Swift Package Manager
- Unit and UI testing using XCTestCase
- Follow the code guidelines from SwiftLint (https://github.com/realm/SwiftLint)
- SOLID principles
- Views created in code (making it easier to transition to SWiftUI in the future)
- Coordinators are responsible for the navigation and Dependency Injection of the MVVM components
- Cache using a a costum cache based on NSCache
- Codable for Json serialization
- Layouts built with snapkit

![Screenshot](/docs/Screenshot1.png)
![Screenshot](/docs/Screenshot2.png)

