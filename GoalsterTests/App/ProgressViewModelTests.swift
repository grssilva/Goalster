//
//  ProgressViewModelTests.swift
//  GoalsterTests
//
//  Created by Guilherme Silva on 02/10/2020.
//

import XCTest

class ProgressViewModelTests: XCTestCase {
    let mockGoalRepository = MockGoalRepository()
    let mockStepsRepository = MockStepsRespository()
    var viewModel: ProgressViewModel!

    override func setUp() {
        super.setUp()
        viewModel = ProgressViewModel(goalRepository: mockGoalRepository, stepsRepository: mockStepsRepository)
    }

    func testCaculateRewards() throws {
        //Values
        let steps: Double = 1000
        let goals = mockGoalRepository.testGoals

        //Expected values
        let expectedResult = [goals[0], goals[1]]

        //Results
        let result = viewModel.getEarnedGoals(steps: steps, allGoals: goals)

        XCTAssertEqual(expectedResult.count, result.count)
        XCTAssertEqual(expectedResult[0], result[0])
        XCTAssertEqual(expectedResult[1], result[1])

        assert(result[0].goal <= steps)
        assert(result[1].goal <= steps)

        XCTAssertFalse(goals[2].goal <= steps)
    }

    func testCaculatePoints() throws {
        //Values
        let goals = mockGoalRepository.testGoals

        //Expected values
        let expectedResult = 11100

        //Results
        let result = viewModel.getPoints(goals: goals)

        XCTAssertEqual(expectedResult, result)
    }

    func testNextGoal() throws {
        //Values
        let steps: Double = 1000
        let goals = mockGoalRepository.testGoals

        //Expected values
        let expectedResult =  goals[2]

        //Results
        let result = viewModel.getNextGoal(steps: steps, allGoals: goals)

        XCTAssertEqual(result, expectedResult )
    }

}
