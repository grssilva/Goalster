//
//  MainViewModelTests.swift
//  GoalsterTests
//
//  Created by Guilherme Silva on 02/10/2020.
//

import XCTest

class MainViewModelTests: XCTestCase {
    let mockGoalRepository = MockGoalRepository()
    let mockStepsRepository = MockStepsRespository()
    var viewModel: MainViewModel!

    override func setUp() {
        viewModel = MainViewModel(goalRepository: mockGoalRepository, stepsRepository: mockStepsRepository)
    }

    func testGoingToProgress_failure() throws {
        let result = viewModel.goToProgress()

        XCTAssertEqual(result, false)
    }

    func testGoingToProgress_success() throws {
        _ = mockStepsRepository.askForPermissions()
        let result = viewModel.goToProgress()

        XCTAssertEqual(result, true)
    }
}
