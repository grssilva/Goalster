//
//  MockStepsRespository.swift
//  GoalsterTests
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import RxSwift
import RxCocoa

class MockStepsRespository: StepsRepository {
    var stepCount = BehaviorRelay<StepsData?>(value: nil)

    private var enabled = false
    func isEnabled() -> Bool {
        return enabled
    }

    func askForPermissions() -> Observable<Bool> {
        enabled = true
        return .just(true)
    }

    func update() {
        enabled = true
    }
}
