//
//  MockGoalRepository.swift
//  GoalsterTests
//
//  Created by Guilherme Silva on 02/10/2020.
//

import Foundation
import RxSwift

class MockGoalRepository: GoalRepository {
    var testGoals: [Goal] {
        let goal1 = Goal(id: "1", title: "Test1",
                         description: "", type: .step, goal: 100,
                         reward: Reward(trophy: .bronzeMedal, points: 100))
        let goal2 = Goal(id: "2", title: "Test2",
                         description: "", type: .step, goal: 999,
                         reward: Reward(trophy: .silverMedal, points: 1000))
        let goal3 = Goal(id: "3", title: "Test3",
                         description: "", type: .step, goal: 10000,
                         reward: Reward(trophy: .goldMedal, points: 10000))
        return [goal1, goal2, goal3]
    }

    func getGoals() -> Observable<[Goal]> {
        return .just(testGoals)
    }
}
