//
//  GoalApiTests.swift
//  GoalsterTests
//
//  Created by Guilherme Silva on 02/10/2020.
//

import XCTest

class GoalApiTests: XCTestCase {
    func testJsonMappingCount() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "ApiTest", withExtension: "json") else {
            XCTFail("Missing file: User.json")
            return
        }

        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        let response = try decoder.decode(GetGoalsResponse.self, from: data)

        // Check of we have the 6 items
        XCTAssertEqual(response.items.count, 6)
    }

    func testJsonMappingFidelity() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "ApiTest", withExtension: "json") else {
            XCTFail("Missing file: User.json")
            return
        }

        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        let response = try decoder.decode(GetGoalsResponse.self, from: data)

        // Check if the first item is intact
        let firstItem = response.items[0]
        XCTAssertEqual(firstItem.id, "1000")
        XCTAssertEqual(firstItem.title, "Easy walk steps")
        XCTAssertEqual(firstItem.description, "Walk 500 steps a day")
        XCTAssertEqual(firstItem.type, "step")
        XCTAssertEqual(firstItem.goal, 500)
        XCTAssertEqual(firstItem.reward.points, 5)
        XCTAssertEqual(firstItem.reward.trophy, "bronze_medal")
    }
}
